using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour
{
    [SerializeField] private Sprite[] cardContent;
    [SerializeField] private Sprite cardBack;

    public int cardType = 1;

    private GameplayState gameplayState;
    private Image cardImage;

    public void Start()
    {
        cardImage = GetComponent<Image>();
        gameplayState = FindObjectOfType<GameplayState>();
    }

    public void OnClick()
    {
        if (cardImage.sprite.Equals(cardBack))
        {
            cardImage.sprite = cardContent[cardType];
            gameplayState.Guess(this);
        }
        else
        {
            cardImage.sprite = cardBack;
        }
    }

    public void SetDefaultImage()
    {
        cardImage.sprite = cardBack;
    }
}
