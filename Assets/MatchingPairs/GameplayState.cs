using UnityEngine;
using UnityEngine.UI;

public class GameplayState : MonoBehaviour
{
    [SerializeField] private Card[] cards;
    [SerializeField] private Text scoreText;
    [SerializeField] private int score = 0;

    private Card firstCard;
    private Card secondCard;

    private int[] cardTypes = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4}; 

    public void Start()
    {
        SetScoreText(score);
        ShuffleCards();
    }

    public void OnRestartClicked()
    {
        foreach (var card in cards)
        {
            card.SetDefaultImage();
        }
        ShuffleCards();
    }

    public void Guess(Card card)
    {
        if (firstCard == null)
        {
            firstCard = card;
        }
        else if (secondCard == null)
        {
            secondCard = card;
            Check();
        }
        else
        {
            firstCard.SetDefaultImage();
            secondCard.SetDefaultImage();

            firstCard = null;
            secondCard = null;

            Guess(card);
        }
    }
    
    private void SetCardType()
    {
        for (int i = 0; i < cards.Length; i++)
        {
            cards[i].cardType = cardTypes[i];
        }
    }
    
    private void SetScoreText(int score)
    {
        scoreText.text = $"Счёт: {score}";
    }

    private void ShuffleCards()
    {
        //Fisher–Yates shuffle
        System.Random rnd = new System.Random();
        for (int i = cardTypes.Length - 1; i >= 1; i--)
        {
            int j = rnd.Next(i + 1);

            int temp = cardTypes[j];
            cardTypes[j] = cardTypes[i];
            cardTypes[i] = temp;
        }
        
        SetCardType();
    }
    
    private void Check()
    {
        if (firstCard.cardType == secondCard.cardType)
        {
            score++;
            firstCard = null;
            secondCard = null;
            SetScoreText(score);
        }
        else
        {
            score--;
            SetScoreText(score);
        }
    }
}
